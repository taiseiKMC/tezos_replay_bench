set datafile separator ","

set term svg
set output report_dir."/rss.svg"

set xlabel 'Block Level'
set ylabel 'Giga Byte'

set format x "%gk"

plot report_dir."/replays.csv" using ($1/1000):3 with lines linecolor rgb "dark-violet" title "RSS", \
                            "" using ($1/1000):4 with lines linecolor rgb "#009e73" title "Max RSS"
